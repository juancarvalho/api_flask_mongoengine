#  SIMPLE FLASK API

### Details:
	This api was a test requested in a selection process of a company in 2020/07.
	
	author: juan.carvalho82@gmail.com
	LinkedIn: https://www.linkedin.com/in/juan-carvalho/
	Medium: https://medium.com/@juan.carvalho82
	
### Main technologies used in project:
	mongoengine
	flask_jwt_extended (token access)
	python 3.8
	Docker
	MongoDB

### Wiki
[https://bitbucket.org/juancarvalho/api_flask_mongoengine/wiki/Home]


### Clone and up containers:
	$ git clone https://juancarvalho@bitbucket.org/juancarvalho/api_flask_mongoengine.git
	$ cd api_flask_mongoengine
	$ docker-compose up

### Usage:
	$ curl -XGET 'http://localhost:5000/api/v1/status'
	{
	  "msg": "Documentation url: /api/v1/doc", 
	  "success": true
	}
	
### Access MongoDb GUI:
	http://localhost:8081

### Documentation:
	# In your browser
	http://localhost:5000/api/v1/doc

### Python client example:
	import requests
	def test_create_user(self):
	    url = "http://localhost:5000/api/v1/createuser"
	    data = {'username': 'juancarvalho', 'password': 'abc123'}
	    response = requests.post(url, json=data)
	    print(response.json())
	    print(response.status_code)

	# Example With Token Access:
	def test_delete_user(self):
	    url = "http://localhost:5000/api/v1/deleteuser"
	    data = {'username': 'juancarvalho', 'password': 'abc123'}
		headers = {'Authorization': 'Bearer {}'.format(access_token)}
	    response = requests.post(url, json=data, headers=headers)
	    print(response.json())
	    print(response.status_code)

### To run tests:
	$ cd api_flask_mongoengine_poetry/api_flask
	$ python -m unittest discover tests

	3.8.0 (default, Oct 28 2019, 16:14:01) 
	[GCC 8.3.0]
	{'success': True}
	{'success': True}
	{'success': True}
	{'success': True}
	{'success': True}
	{'success': True}
	{'success': True}

	----------------------------------------------------------------------
	Ran 7 tests in 0.081s

	OK
