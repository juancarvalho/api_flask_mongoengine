#!/usr/bin/env python
# coding: utf-8
import requests
import json
from urllib.parse import urljoin
import configparser

# Get config vars ===========================================
config = configparser.ConfigParser()  # Init config object
config.read('config/settings.ini')  # Set .ini file
env = config.get('environment', 'current')  # Get current environment
url_server = config.get(env, 'url_server')
create_url = config.get(env, 'create_url')
edit_url = config.get(env, 'edit_url')
get_url = config.get(env, 'get_url')


def insert(*args, **kwargs):
    """
        Create a post request to insert data in database
        Usage:
            insert(data='29/05/2019', valor='16566')
            insert(**{'data': '29/05/2019', 'valor': '16566'})
            insert(data='29/05/2019', **{'valor': '16566'})

        ARGS:
            keyword arg or dict

        RETURN:
            json object
    """
    url = urljoin(url_server, get_url)
    return_insert = requests.get(url, json=kwargs)
    print(return_insert)
    print(return_insert.json())
    # return return_insert.json()


if __name__ == '__main__':
    insert()
