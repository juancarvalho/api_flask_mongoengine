#!/usr/bin/env python
# coding: utf-8
from werkzeug.security import generate_password_hash
from mongoengine import *

# connect(db='bar', host='mongo', alias='default')


class User(Document):
    """
        Template for a mongoengine document, which represents a user.
        Password is automatically hashed before saving.

        :param username: unique required email-string value
        :param password: required string value, longer than 6 characters

        :Example:
        >>> new_user = Users(username="juancarvalho", password="hunter2")
        >>> new_user.save()

        >>> new_user.name = "spammy"
        >>> new_user.save()
        # Remove test user
        >>> new_user.delete()
    """
    username = StringField(required=True, max_length=200)
    password = StringField(required=True)
    meta = {
        'allow_inheritance': True,
    }

    def hash_password(self, password):
        self.password = generate_password_hash(password)


class Product(Document):
    """docstring for ClassName"""
    name = StringField(required=True, max_length=200)


class Order(Document):
    """docstring for ClassName"""
    product = ReferenceField(Product, reverse_delete_rule=CASCADE)
    user = ReferenceField(User, reverse_delete_rule=CASCADE)
