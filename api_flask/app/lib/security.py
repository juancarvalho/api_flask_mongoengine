from werkzeug.security import check_password_hash
import os
import sys
root_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(root_dir)
print(root_dir)
from models.bar import User, DoesNotExist


def authenticate(username, password):
    """
        Authenticate user

        ARGS:
            username: unique required string value
            password: unique required string value
        RETURN:
            Bool
    """
    try:
        user = User.objects.get(username=username)
        if user and check_password_hash(user.password, password):
            return user
        else:
            return False
    except DoesNotExist as e:
        return False
    except Exception as e:
        raise e
