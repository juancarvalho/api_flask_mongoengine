from flask import jsonify, request
from functools import wraps


def required_data(*args_req):
    """
        Search if receive required data

        ARGS:
            args_req > str required values
        USAGE:
            @required_data(app, "username")
            my_function()

        RETURN:
            function() or json object
    """
    def decorator(func):
        @wraps(func)
        def wrapped(*args, **kwargs):
            if not request.is_json:
                return jsonify({"msg": "Missing JSON in request"}), 400
            keys = request.json.keys()  # Received keys in request
            for index, item in enumerate(args_req):
                if item not in keys:
                    return jsonify(
                        success=False,
                        msg='{} required.'.format(item)
                    ), 405
            return func(*args, **kwargs)
        return wrapped
    return decorator
