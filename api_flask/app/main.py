from flask import Flask, jsonify, request
from flask_mongoengine import MongoEngine
from .models.bar import User, Product, Order, DoesNotExist
from .lib.security import authenticate
from .lib.decorators import required_data
from flask_selfdoc import Autodoc
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token
)

# Init flask object ==================================================
app = Flask(__name__)
SECRET_KEY = 'sdafdkjhsfmcvbbasjjsafghghgsdkja'
app.config['SECRET_KEY'] = SECRET_KEY
app.config['MONGODB_SETTINGS'] = {
    'alias': 'default',
    'db': 'bar',
    'host': 'mongo',
}
jwt = JWTManager(app)
db = MongoEngine(app)
auto = Autodoc(app)


@app.route('/api/v1/createuser', methods=['POST'])
@auto.doc(args=['json { username: string, password: string }'])
@required_data('username', 'password')
def createuser():
    """ Create user if not exists
        return: json { success: bool, msg: str }
    """
    username = request.json.get('username', None)
    password = request.json.get('password', None)
    try:
        User.objects.get(username=username)
        return jsonify(
            success=False, msg="User {} already exists".format(username)
        ), 409
    except User.DoesNotExist:
        user = User(username=username)
        user.hash_password(password)
        user.save()
    return jsonify(
        success=True, msg='User {} created successfully'.format(username)
    ), 201


@app.route('/api/v1/login', methods=['POST'])
@auto.doc(args=['json { username: string, password: string }'])
@required_data('username', 'password')
def login():
    """ Authenticate user and return access token.
        return: json { success: bool, access_token: str, msg: str }
    """
    username = request.json.get('username', None)
    password = request.json.get('password', None)
    if not authenticate(username, password):
        return jsonify(
            success=False,
            access_token='',
            msg="Bad username or password"), 401
    # Identity can be any data that is json serializable
    access_token = create_access_token(identity=username)
    return jsonify(
        success=True,
        access_token=access_token,
        msg="Token generate"), 200


@app.route('/api/v1/deleteuser', methods=['DELETE'])
@auto.doc(args=['json { username: string, password: string }'])
@jwt_required
@required_data('username')
def deleteuser():
    """ Delete user in database.
        Attention, if you delete this object,
        all orders related to it will also be deleted,
        be careful!
        Access Token Required in Header
        return: json { success: bool, msg: str }
    """
    username = request.json.get('username', None)
    try:
        user_obj = User.objects.get(username=username)
        user_obj.delete()
    except DoesNotExist as e:
        return jsonify(
            success=False,
            msg="User {} not exists".format(username)
        ), 400
    except Exception as e:
        raise e
    return jsonify(success=True, msg="User {} deleted".format(username)), 200


@app.route('/api/v1/createproduct', methods=['POST'])
@auto.doc(args=['json { product: string }'])
@required_data('product')
@jwt_required
def createproduct():
    """ Create product if not exists.
        Access Token Required in Header
        return: json { success: bool, msg: str }
    """
    product = request.json.get('product', None)
    try:
        Product.objects.get(name=product)
        return jsonify(
            success=False, msg="Product {} already exists".format(product)
        ), 409
    except Product.DoesNotExist:
        product_obj = Product(name=product)
        product_obj.save()
    return jsonify(
        success=True,
        msg="Product {} created successfully.".format(product)
    ), 200


@app.route('/api/v1/deleteproduct', methods=['DELETE'])
@auto.doc(args=['json { product: string }'])
@required_data('product')
@jwt_required
def deleteproduct():
    """ Delete product.
        Attention, if you delete this object,
        all orders related to it will also be deleted,
        be careful!
        Access Token Required in Header
        return: json { success: bool, msg: str }
    """
    product = request.json.get('product', None)
    try:
        product_obj = Product.objects.get(name=product)
        product_obj.delete()
    except DoesNotExist as e:
        return jsonify(
            success=False,
            msg="Product {} not exists".format(product)
        ), 409
    except Exception as e:
        raise e
    return jsonify(success=True, msg="Product {} deleted".format(product)), 200


@app.route('/api/v1/orderproduct', methods=['POST'])
@auto.doc(args=['json { username: string, product: string }'])
@jwt_required
def orderproduct():
    """ Create product order.
        Access Token Required in Header
        return: json { success: bool, msg: str }
    """
    username = request.json.get('username', None)
    product = request.json.get('product', None)
    try:
        user_obj = User.objects.get(username=username)
    except DoesNotExist as e:
        return jsonify(
            success=False,
            msg="User {} not exists".format(username)
        ), 409
    except Exception as e:
        raise e

    try:
        product_obj = Product.objects.get(name=product)
    except DoesNotExist as e:
        return jsonify(
            success=False,
            msg="Product {} not exists".format(product)
        ), 409
    except Exception as e:
        raise e

    order_obj = Order(user=user_obj, product=product_obj)
    order_obj.save()
    return jsonify(
        success=True,
        msg="Order product {} created successfully".format(product)
    ), 200


@app.route('/api/v1/deleteorders', methods=['DELETE'])
@auto.doc(args=['json { username: string }'])
@jwt_required
@required_data('username')
def deleteorders():
    """ Delete all orders of user
        Access Token Required in Header
        return: json { success: bool, msg: str }
    """
    username = request.json.get('username', None)
    try:
        user_obj = User.objects.get(username=username)
    except DoesNotExist as e:
        return jsonify(
            success=False,
            msg="User {} not exists".format(username)
        ), 409
    except Exception as e:
        raise e
    order_deleted = Order.objects(user=user_obj).delete()
    return jsonify(
        success=True,
        msg="{} products deleted from user {}".format(order_deleted, username)
    ), 200


@app.route('/api/v1/getorders', methods=['GET'])
@auto.doc(args=['json { username: string }'])
@jwt_required
@required_data('username')
def getorders():
    """ Return all orders of user.
        Access Token Required in Header
        return: json { success: bool, products: json, msg: str }
    """
    username = request.json.get('username', None)
    try:
        user_obj = User.objects.get(username=username)
    except DoesNotExist as e:
        return jsonify(
            success=False,
            products={},
            msg="User {} not exists".format(username)
        ), 409
    except Exception as e:
        raise e
    orders_list = Order.objects(user=user_obj).only('product').exclude('id')
    # app.logger.info(orders_list)
    return jsonify(
        success=True,
        products=orders_list.to_json(),
        msg="List products returned"
    ), 200


@app.route('/api/v1/status', methods=['GET'])
@auto.doc()
def status():
    """ Check server status
        Access Token Required in Header
        return: json { success: bool }
    """
    return jsonify(success=True, msg="Documentation url: /api/v1/doc"), 200


@app.route('/api/v1/doc')
def public_doc():
    return auto.html(title='Simple Documentation API')


if __name__ == '__main__':
    app.run(debug=True)
