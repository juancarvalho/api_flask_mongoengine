import unittest
import sys
import configparser
import os
from urllib.parse import urljoin
import requests
root_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(root_dir)
# from app.main import app

# Get root dir

# Get config vars ===========================================
config = configparser.ConfigParser()  # Init config object
config.read(os.path.join(root_dir, 'app/config/settings.ini'))  # Set .ini file
env = config.get('environment', 'current')  # Get current environment
url_server = config.get(env, 'url_server')
login_url = config.get(env, 'login_url')
createuser_url = config.get(env, 'createuser_url')
deleteuser_url = config.get(env, 'deleteuser_url')
createproduct_url = config.get(env, 'createproduct_url')
deleteproduct_url = config.get(env, 'deleteproduct_url')
orderproduct_url = config.get(env, 'orderproduct_url')
deleteorders_url = config.get(env, 'deleteorders_url')
getorders_url = config.get(env, 'getorders_url')
status_url = config.get(env, 'status_url')


class TestHome(unittest.TestCase):

    access_token = 'None'

    def test_create_user(self):
        url = "http://localhost:5000/api/v1/createuser"
        data = {'username': 'juan', 'password': '12345abc'}
        response = requests.post(url, json=data)
        print(response.json())
        self.assertEqual(201, response.status_code)

    def test_login(self):
        url = urljoin(url_server, login_url)
        data = {'username': 'juan', 'password': '12345abc'}
        response = requests.post(url, json=data)
        print(response.json())
        self.access_token = response.json()['access_token']
        self.assertEqual(200, response.status_code)

    def test_delete_user(self):
        url = urljoin(url_server, deleteuser_url)
        data = {'username': 'juan'}
        headers = {'Authorization': 'Bearer {}'.format(self.access_token)}
        response = requests.delete(url, json=data, headers=headers)
        print(response.json())
        self.assertEqual(200, response.status_code)

    def test_create_product(self):
        url = urljoin(url_server, createproduct_url)
        data = {'product': 'coca cola'}
        headers = {'Authorization': 'Bearer {}'.format(self.access_token)}
        response = requests.post(url, json=data, headers=headers)
        print(response.json())
        self.assertEqual(200, response.status_code)

    def test_delete_product(self):
        url = urljoin(url_server, deleteproduct_url)
        data = {'product': 'coca cola'}
        headers = {'Authorization': 'Bearer {}'.format(self.access_token)}
        response = requests.delete(url, json=data, headers=headers)
        print(response.json())
        self.assertEqual(200, response.status_code)

    def test_order_product(self):
        url = urljoin(url_server, orderproduct_url)
        data = {'username': 'juan', 'product': 'coca cola'}
        headers = {'Authorization': 'Bearer {}'.format(self.access_token)}
        response = requests.post(url, json=data, headers=headers)
        print(response.json())
        self.assertEqual(200, response.status_code)

    def test_deleteorder(self):
        url = urljoin(url_server, deleteorders_url)
        data = {'username': 'juan'}
        headers = {'Authorization': 'Bearer {}'.format(self.access_token)}
        response = requests.delete(url, json=data, headers=headers)
        print(response.json())
        self.assertEqual(200, response.status_code)

    def test_getorders(self):
        url = urljoin(url_server, getorders_url)
        data = {'username': 'juan'}
        headers = {'Authorization': 'Bearer {}'.format(self.access_token)}
        response = requests.get(url, json=data, headers=headers)
        print(response.json())
        self.assertEqual(200, response.status_code)

    def test_status(self):
        url = urljoin(url_server, status_url)
        headers = {'Authorization': 'Bearer {}'.format(self.access_token)}
        data = {'nome': 'juancarvalho'}
        response = requests.get(url, headers=headers, json=data)
        print(response.json())
        self.assertEqual(200, response.status_code)


if __name__ == '__main__':
    obj = TestHome()
    obj.test_create_user()
    # obj.test_login()
    # obj.test_create_product()
    # obj.test_order_product()
    # obj.test_getorders()
    # obj.test_deleteorder()
    # obj.test_delete_product()
    # obj.test_delete_user()
